# lottery_彩票系统

#### 介绍
lottery彩票系统,前端和后端基本实现,包含门户网站,后台管理,手机移动端,还需要对剩下的支付接口和页面做调整,牧犬使用security验证登录,正在接入oauth2使用第三方登录,具体还在优化,小伙伴们期待更新吧,

#### 软件架构

springboot+vue+jpa,security,等技术,

#### 安装教程

1.  使用springboot的run dashbord 启动  自动会读取存在的启动类
2.  遇到问题  或者需要技术交流,请添加微信 vip894941971  备注一下
3.  记得star   Thanks 
4.  个人博客 欢迎来踩 [lqp9595.com](http://lqp9595.com)

#### 使用说明

1.  即将上线演示环境

#### 截图   全是一体化


手机移动端,页面自适应
![手机移动端,自适应](https://images.gitee.com/uploads/images/2020/1023/140711_5fb4b94f_7727235.png "Snipaste_2020-10-23_12-48-51.png")



手机移动端,自适应
![手机移动端,自适应](https://images.gitee.com/uploads/images/2020/1023/140724_fc632d70_7727235.png "Snipaste_2020-10-23_12-49-02.png")




手机移动端,自适应
![手机移动端,自适应](https://images.gitee.com/uploads/images/2020/1023/140735_49901a91_7727235.png "Snipaste_2020-10-23_12-49-16.png")




PC端后台管理
![PC端后台管理](https://images.gitee.com/uploads/images/2020/1023/140747_de0a075c_7727235.png "Snipaste_2020-10-23_12-50-17.png")




PC端门户网站
![PC端门户网站](https://images.gitee.com/uploads/images/2020/1023/140757_e5dea222_7727235.png "Snipaste_2020-10-23_12-50-36.png")



#### 特技

1.  由于程序不断优化，界面细节可能有所变化，请以实际页面为准
2.  但是页面会越来越好
